#include "trajectory_generation.h"
#include <iostream>

Polynomial::Polynomial(){};

Polynomial::Polynomial(const double &piIn, const double &pfIn, const double &DtIn)
{
  pi = piIn;                 // def initial pos
  pf = pfIn;                 // def final pos
  Dt = DtIn;                 // def final time
  a[0] = pi;                 // def a0
  a[1] = 0;                  // def a1
  a[2] = 0;                  // def a2
  a[3] = 10 * pf - 10 * pi;  // def a3
  a[4] = -15 * pf + 15 * pi; // def a4
  a[5] = 6 * pf - 6 * pi;    // def a5
};

void Polynomial::update(const double &piIn, const double &pfIn, const double &DtIn)
{
  pf = pfIn; // def final pos
  pi = piIn; // def initial pos
  Dt = DtIn; // def final time

  a[0] = pi;                 // def a0
  a[1] = 0;                  // def a1
  a[2] = 0;                  // def a2
  a[3] = 10 * pf - 10 * pi;  // def a3
  a[4] = -15 * pf + 15 * pi; // def a4
  a[5] = 6 * pf - 6 * pi;    // def a5
};

const double Polynomial::p(const double &t)
{
  const double p = a[0] + a[1] * pow(t / Dt, 1) + a[2] * pow(t / Dt, 2) + a[3] * pow(t / Dt, 3) + a[4] * pow(t / Dt, 4) + a[5] * pow(t / Dt, 5); // def polynome t/dt -> t=dt xd = xf
  return p;
};

const double Polynomial::dp(const double &t)
{
  const double dp = 3 * a[3] * pow(t / Dt, 2) / Dt + 4 * a[4] * pow(t / Dt, 3) / Dt + 5 * a[5] * pow(t / Dt, 4) / Dt; // def deriv of pol -> velocity
  return dp;
};

Point2Point::Point2Point(const Eigen::Affine3d &X_i, const Eigen::Affine3d &X_f, const double &DtIn)
{
  polx.update(X_i(0, 3), X_f(0, 3), DtIn);
  poly.update(X_i(1, 3), X_f(1, 3), DtIn);
  polz.update(X_i(2, 3), X_f(2, 3), DtIn);

  Eigen::Matrix3d Rot_i = X_i.linear();
  Eigen::Matrix3d Rot_f = X_f.linear();
  Eigen::Matrix3d Rif = Rot_f * Rot_i.transpose();

  Eigen::AngleAxisd R(Rif);
  axis = R.axis();
  pol_angle.update(0, R.angle(), DtIn);
}

Eigen::Affine3d Point2Point::X(const double &time)
{
  Eigen::Affine3d X = Eigen::Affine3d::Identity();
  // translation
  X(0, 3) = polx.p(time);
  X(1, 3) = poly.p(time);
  X(2, 3) = polz.p(time);

  // rotaion
  Eigen::AngleAxisd R = Eigen::AngleAxisd(pol_angle.p(time), axis);
  X.linear() = R.matrix();
  return X;
}

Eigen::Vector6d Point2Point::dX(const double &time)
{
  Eigen::Vector6d X;
  X(0) = polx.dp(time);
  X(1) = poly.dp(time);
  X(2) = polz.dp(time);
  X(3) = axis(0) * pol_angle.dp(time);
  X(4) = axis(1) * pol_angle.dp(time);
  X(5) = axis(2) * pol_angle.dp(time);
  return X;
}