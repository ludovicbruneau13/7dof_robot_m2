#include "control.h"
#include <iostream>
Controller::Controller(RobotModel &rmIn) : model(rmIn)
{
}

Eigen::Vector7d Controller::Dqd(
    const Eigen::Vector7d &q,
    const Eigen::Affine3d &xd,
    const Eigen::Vector6d &Dxd_ff)
{
  model.FwdKin(X, J, q);

  // partie translation
  dX_desired(0) = (xd(0, 3) - X(0, 3)) * kp + Dxd_ff(0);
  dX_desired(1) = (xd(1, 3) - X(1, 3)) * kp + Dxd_ff(1);
  dX_desired(2) = (xd(2, 3) - X(2, 3)) * kp + Dxd_ff(2);

  Eigen::Matrix3d R_des(xd.linear());
  Eigen::Matrix3d R_cur(X.linear());

  Eigen::AngleAxisd Re(R_des * R_cur.transpose());
  Eigen::Vector3d axis = Re.axis();

  dX_desired(3) = kp * Re.angle() * axis(0) + Dxd_ff(3);
  dX_desired(4) = kp * Re.angle() * axis(1) + Dxd_ff(4);
  dX_desired(5) = kp * Re.angle() * axis(2) + Dxd_ff(5);

  Eigen::Matrix<double, 7, 6> J_inv;
  Eigen::Matrix<double, 6, 6> J_inv_T;

  J_inv_T = J * J.transpose();
  J_inv = J.transpose() * J_inv_T.completeOrthogonalDecomposition().pseudoInverse();

  return J_inv * dX_desired;
}