#include "kinematic_model.h"
#include <iostream>

RobotModel::RobotModel(const std::array<double, 7> &aIn, const std::array<double, 7> &dIn, const std::array<double, 7> &alphaIn) : a{0, 0, 0, 0.0825, -0.0825, 0, 0.0880},
                                                                                                                                   d{0.3330, 0, 0.3160, 0, 0.3840, 0, 0},
                                                                                                                                   alpha{0, -M_PI_2, M_PI_2, M_PI_2, -M_PI_2, M_PI_2, M_PI_2} {/*std::cout<< "Initialisation kinematic\n\n\n";*/};

void RobotModel::FwdKin(Eigen::Affine3d &xOut, Eigen::Matrix67d &JOut, const Eigen::Vector7d &qIn)
{

  Eigen::Matrix4d T;
  Eigen::Matrix<double, 3, 7> P;
  Eigen::Matrix<double, 3, 7> Z;
  Eigen::Matrix<double, 3, 7> P_diff;

  xOut = xOut.Identity();

  for (int i = 0; i < 7; i++)
  {
    T << cos(qIn[i]), -sin(qIn[i]), 0, a[i],
        cos(alpha[i]) * sin(qIn[i]), cos(alpha[i]) * cos(qIn[i]), -sin(alpha[i]), -d[i] * sin(alpha[i]),
        sin(alpha[i]) * sin(qIn[i]), sin(alpha[i]) * cos(qIn[i]), cos(alpha[i]), d[i] * cos(alpha[i]),
        0, 0, 0, 1;

    xOut.matrix() = xOut.matrix() * T;
    P.col(i) = xOut.translation();
    Z.col(i) = xOut.linear().col(2);
  }

  for (int i = 0; i < 7; i++)
  {
    P_diff.col(i) = Z.col(i).cross(P.col(6) - P.col(i));
  }
  JOut << P_diff, Z;

  Eigen::Matrix3d I = Eigen::Matrix3d::Identity();
  Eigen::Matrix3d O = Eigen::Matrix3d::Zero();
  Eigen::MatrixXd IO(I.rows() + I.rows(), I.cols());
  Eigen::Matrix3d D;
  Eigen::Matrix3d C = Eigen::Matrix3d::Identity();
  Eigen::MatrixXd DC(D.rows() + C.rows(), D.cols());
  Eigen::MatrixXd IODC(IO.rows(), IO.cols() + DC.cols());
  Eigen::Matrix3d R;
  double r8 = 0.1070;
  R = xOut.linear();
  D << 0, a[6] * R(2, 0) + r8 * R(2, 2), -a[6] * R(1, 0) - r8 * R(1, 2),
      -a[6] * R(2, 0) - r8 * R(2, 2), 0, a[6] * R(0, 0) + r8 * R(0, 2),
      a[6] * R(1, 0) + r8 * R(1, 2), -a[6] * R(0, 0) - r8 * R(0, 2), 0;
  IO << I, O;
  DC << D, C;
  IODC << IO, DC;
  JOut = IODC * JOut;
}