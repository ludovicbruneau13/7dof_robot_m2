#include "trajectory_generation.h"
#include <iostream>

using namespace std;

namespace Eigen
{
  typedef Matrix<double, 6, 1> Vector6d;
}
int main()
{

  double t;
  double dt = 10e-2;
  const double Tf = 10.00;

  Eigen::Affine3d X_i = Eigen::Affine3d::Identity();
  Eigen::Affine3d X_f = Eigen::Affine3d::Identity();
  Eigen::Matrix3d Rot;
  Eigen::Vector3d axis(1, 0, 0);

  Rot << 1, 0, 0,
      0, -1, 0,
      0, 0, -1;
  X_i.linear() = Rot;
  axis << 0.088, 0, 1.033;
  X_i.translation() = axis;

  Eigen::AngleAxisd R(M_PI_2, axis);
  X_f.linear() = R.matrix();
  axis << 0, 0.088, 1.033;
  X_f.translation() = axis;

  Eigen::Affine3d X = Eigen::Affine3d::Identity();
  Eigen::Vector6d dX{(0, 0, 0, 0, 0, 0)};

  Point2Point MyTrajectory(X_i, X_f, Tf);
  cout << "t, x, y, z, angle, xd_x, xd_y, xd_z, w_x, w_y, w_z"
       << "\n"; // Plot's variables

  for (t = 0; t <= Tf; t += dt)
  {
    X = MyTrajectory.X(t);
    dX = MyTrajectory.dX(t);
    Eigen::AngleAxisd X_orientation(X.linear());
    cout << t << "," << X(0, 3) << "," << X(1, 3) << "," << X(2, 3) << "," << X_orientation.angle() << "," << dX(0) << "," << dX(1) << "," << dX(2) << "," << dX(3) << "," << dX(4) << "," << dX(5) << endl; // print variables
  }
}