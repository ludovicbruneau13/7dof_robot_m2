#include "control.h"
#include "trajectory_generation.h"
#include <iostream>

int main()
{

  const std::array<double, 7> a{0, 0, 0, 0.0825, -0.0825, 0, 0.0880};
  const std::array<double, 7> d{0.3330, 0, 0.3160, 0, 0.3840, 0, 0};
  const std::array<double, 7> alpha{0, -M_PI_2, M_PI_2, M_PI_2, -M_PI_2, M_PI_2, M_PI_2};

  double t = 0;
  double dt = 10e-4;
  const double Tf = 10.00;

  Eigen::Affine3d Xi = Eigen::Affine3d::Identity();
  Eigen::Affine3d Xf = Eigen::Affine3d::Identity();

  Eigen::Affine3d xOut = Eigen::Affine3d::Identity();

  RobotModel MyRobot(a, d, alpha);
  Controller MyController(MyRobot);

  Eigen::Matrix67d J;
  Eigen::Vector7d q;

  // Etat final desiré
  q << M_PI_2, M_PI_2, M_PI_2, M_PI_2, M_PI_2, M_PI_2, M_PI_2;
  MyRobot.FwdKin(Xf, J, q);

  // Etat initial
  q << 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0;
  MyRobot.FwdKin(Xi, J, q);

  Point2Point MyTrajectory(Xi, Xf, Tf);

  Eigen::Affine3d xd;
  Eigen::Affine3d x = Eigen::Affine3d::Identity();
  Eigen::Vector6d Dxd_ff;
  Eigen::Vector3d Euler_angle_desired;
  Eigen::Vector3d Euler_angle_current;
  Eigen::Vector7d q_dot;
  Eigen::Vector3d axis;
  std::cout << "t, xd_x, xd_y, xd_z, x, y, z"
            << "\n"; // Plot's variables
  x = Xi;
  while (t < Tf)
  {
    MyRobot.FwdKin(x, J, q);
    xd = MyTrajectory.X(t);
    Dxd_ff = MyTrajectory.dX(t);
    q_dot = MyController.Dqd(q, xd, Dxd_ff);
    q = q + q_dot * dt;
    std::cout << t << "," << xd(0, 3) << "," << xd(1, 3) << "," << xd(2, 3) << "," << x(0, 3) << "," << x(1, 3) << "," << x(2, 3) << "\n"; // Plot's variables
    t = t + dt;
  }
}