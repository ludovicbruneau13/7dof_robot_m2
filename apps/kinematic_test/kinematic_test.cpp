#include "kinematic_model.h"
#include <iostream>

int main()
{

  const std::array<double, 7> a{0, 0, 0, 0.0825, -0.0825, 0, 0.0880};
  const std::array<double, 7> d{0.3330, 0, 0.3160, 0, 0.3840, 0, 0};
  const std::array<double, 7> alpha{0, -M_PI_2, M_PI_2, M_PI_2, -M_PI_2, M_PI_2, M_PI_2};
  RobotModel MyRobot(a, d, alpha);

  Eigen::Affine3d xOut = Eigen::Affine3d::Identity();
  Eigen::Matrix67d JOut;
  Eigen::Vector7d qIn;
  qIn << 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0;

  double i = 0.0;
  std::cout << "t, x, y, z"
            << "\n"; // Plot's variables
  while (M_PI_2 - i > 0)
  {
    qIn << i, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0;
    MyRobot.FwdKin(xOut, JOut, qIn);
    std::cout << i << "," << xOut(0, 3) << "," << xOut(1, 3) << "," << xOut(2, 3) << "\n"; // Plot's variables
    i += 0.001;
  }
}
